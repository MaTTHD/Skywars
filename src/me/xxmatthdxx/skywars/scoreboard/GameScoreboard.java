package me.xxmatthdxx.skywars.scoreboard;

import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.skywars.events.Death;
import me.xxmatthdxx.skywars.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class GameScoreboard {
	
	public static void setGameScoreboard(Player p) {
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        final Scoreboard board = manager.getNewScoreboard();

        final Objective objective = board.registerNewObjective("Nebular", "Nebular");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "● IN GAME ●");


        Score top = objective.getScore(" ");
        Score score1 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Players:");
        Score score2 = objective.getScore(ChatColor.GREEN + "" + GameManager.getInstance().getAlive().size() + ChatColor.GRAY + "/" + ChatColor.RED + Bukkit.getMaxPlayers());
        Score space = objective.getScore("    ");
        Score score3 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Current Kills:");
        Score score4 = objective.getScore(ChatColor.GOLD + "" + Death.getTotalGameKills(p)); //Method for getting current kills
        Score space1 = objective.getScore("  ");
        Score score5 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Time Left:");
        Score score6 = objective.getScore(ChatColor.GOLD + "9m 24s"); //Fix so gets time left
        Score space2 = objective.getScore("        ");
        Score score7 = objective.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Chest Refill:");
        Score score8 = objective.getScore(ChatColor.GOLD + "2m 09s"); //Get time left for refill

        top.setScore(12);
        score1.setScore(11);
        score2.setScore(10);
        space.setScore(9);
        score3.setScore(8);
        score4.setScore(7);
        space1.setScore(6);
        score5.setScore(5);
        score6.setScore(4);
        space2.setScore(3);
        score7.setScore(2);
        score8.setScore(1);

        p.setScoreboard(board);
		
	}

}
