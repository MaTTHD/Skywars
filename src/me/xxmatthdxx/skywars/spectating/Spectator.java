package me.xxmatthdxx.skywars.spectating;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Spectator {
	
	public static ArrayList<Player> spectators = new ArrayList<Player>();
	public static HashMap<String, Integer> compass = new HashMap<String, Integer>();
	
	public static void setSpectatorMode(Player p) {
		spectators.add(p);
		compass.put(p.getName(), 0);
		p.setAllowFlight(true);
		p.setFlying(true);
		for (Player online : Bukkit.getOnlinePlayers()) {
			online.hidePlayer(p); //Hides spectator
		}
		
		p.setHealth(20);
		p.setCanPickupItems(false);
		Inventory inv = p.getInventory();
		
		ItemStack compass = new ItemStack(Material.COMPASS);
		ItemMeta compassMeta = compass.getItemMeta();
		compassMeta.setDisplayName(ChatColor.GOLD + "★" + ChatColor.GREEN + "Teleport! " + ChatColor.GOLD + "★");
		compassMeta.setLore(Arrays.asList(
				ChatColor.GRAY + "",
				ChatColor.GRAY + "" + ChatColor.ITALIC + "Right click to teleport to players!",
				ChatColor.GRAY + ""));
		compass.setItemMeta(compassMeta);
		
		
		ItemStack grass = new ItemStack(Material.GRASS);
		ItemMeta grassMeta = grass.getItemMeta();
		grassMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "LEAVE GAME →");
		grassMeta.setLore(Arrays.asList(
				ChatColor.GRAY + "",
				ChatColor.GRAY + "" + ChatColor.ITALIC + "Right click to leave the game!",
				ChatColor.GRAY + ""));
		grass.setItemMeta(grassMeta);
		
		inv.setItem(0, compass);
		inv.setItem(8, grass);
	}
	
	public static boolean isSpectator(Player p) {
		if (spectators.contains(p)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static HashMap<String, Integer> getCompass() {
		return compass;
	}
}
