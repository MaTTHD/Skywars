package me.xxmatthdxx.skywars.team;

import me.xxmatthdxx.skywars.game.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2015-09-27.
 */
public class TeamManager {

    private static TeamManager instance = new TeamManager();

    private List<Team> teams = new ArrayList<>();

    public static TeamManager getInstance() {
        return instance;
    }

    public boolean isOnSameTeam(Player pl, Player other) {
        for (Team t : teams) {
            if (t.getPlayers().contains(pl.getName()) && t.getPlayers().contains(other.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean hasTeam(Player pl) {
        for (Team team : teams) {
            if (team.getPlayers().contains(pl.getName())) {
                return true;
            }
        }
        return false;
    }

    public List<Team> getTeams(){
        return teams;
    }

    public Team getTeam(Player pl){
        for(Team t : teams){
            if(t.getPlayers().contains(pl.getName())){
                return t;
            }
        }
        return null;
    }

    public void addToTeam(Team team, Player pl) {
        if (team.getPlayers().size() >= 2) {
            pl.sendMessage(ChatColor.RED + "The team is full!");
            return;
        }

        team.getPlayers().add(pl.getName());
        pl.sendMessage(ChatColor.GREEN + "Joined team " + team.getName());
    }

    int spawnIndex = 0;
    int teamIndex = 0;

    public void setup() {
        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (!hasTeam(pl)) {
                Team pTeam = new Team(pl.getName());
                addToTeam(pTeam, pl);
            }
        }
    }

    public void distribute() {

        for (String pl : teams.get(teamIndex).getPlayers()) {
            Player p = Bukkit.getPlayer(pl);
            p.teleport(GameManager.getInstance().getSpawns().get(spawnIndex));
        }
        spawnIndex++;
        teamIndex++;
        if (teams.size() < teamIndex) {
            spawnIndex = 0;
            teamIndex = 0;
            return;
        } else {
            distribute();
        }
    }
}
