package me.xxmatthdxx.skywars.team;

import java.util.List;

/**
 * Created by Matthew on 2015-09-27.
 */
public class Team {
    private String name;
    private List<String> players;

    public Team(String name){
        this.name = name;
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }
}
