package me.xxmatthdxx.skywars.game;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.skywars.Skywars;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Note.Tone;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.scheduler.BukkitRunnable;

public class GameTimer extends BukkitRunnable {

	private Skywars plugin = Skywars.getPlugin();

	public void run() {
		if (plugin.getTicks() > 0) {
			plugin.setTicks(plugin.getTicks() - 1);
		}

		if (plugin.getState() == GameState.JOIN) {
			if (plugin.getTicks() != 0) {
				if (plugin.getTicks() % 30 == 0) {
					Bukkit.broadcastMessage("Game starting in " + plugin.getTicks()); // TODO make message
				}
				else if(plugin.getTicks() <= 5){
					Bukkit.broadcastMessage("Game starting in " + plugin.getTicks());
					for(Player pl : Bukkit.getOnlinePlayers()){
						pl.playNote(pl.getLocation(), Instrument.PIANO, Note.sharp(1, Tone.A));
					}
				}
			}
			else {
				if(Bukkit.getOnlinePlayers().size() < 2){
					Bukkit.broadcastMessage("Game needs at least 2 players to start!");
					plugin.setTicks(60);
					return;
				}
				
				if(GameManager.getInstance().getSpawns().isEmpty()){
					Bukkit.broadcastMessage("There are no spawns!");
					plugin.setTicks(60);
					return;
				}
				GameManager.getInstance().start();
			}
		}
		else if(plugin.getState() == GameState.INGAME){
			if(plugin.getTicks() != 0){
				if(plugin.getTicks() % 30 == 0){
					Bukkit.broadcastMessage("Game ending in " + plugin.getTicks());
				}
				else if(plugin.getTicks() == 60){
					Bukkit.broadcastMessage("1 minute remaining");
				}
				else if(plugin.getTicks() <= 5){
					Bukkit.broadcastMessage("Game ending in " + plugin.getTicks());
					for(Player pl : Bukkit.getOnlinePlayers()){
						pl.playNote(pl.getLocation(), Instrument.PIANO, Note.sharp(1, Tone.A));
					}
				}
			}
			else {
				if(GameManager.getInstance().getAlive().size() > 1){
					plugin.setTicks(2*60);
					new BukkitRunnable(){
						public void run(){
							dropTnt();
						}
					}.runTaskTimer(plugin, 0L, 20*2L);
				}
				else {
					GameManager.getInstance().stop(false);
				}
			}
		}
	}
	
	public void dropTnt(){
		for(Player pl : Bukkit.getOnlinePlayers()){
			TNTPrimed tnt = pl.getWorld().spawn(pl.getLocation().add(0,5,0), TNTPrimed.class);
			tnt.setFuseTicks(2);
			tnt.setVelocity(pl.getLocation().getDirection());
		}
	}
}
