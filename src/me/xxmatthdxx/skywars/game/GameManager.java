package me.xxmatthdxx.skywars.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.skywars.Skywars;
import me.xxmatthdxx.skywars.Particles.ParticleEffect;
import me.xxmatthdxx.skywars.Particles.ParticleEffect.ParticleType;
import me.xxmatthdxx.skywars.events.Death;
import me.xxmatthdxx.skywars.scoreboard.GameScoreboard;

import me.xxmatthdxx.skywars.team.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class GameManager {

    private Skywars plugin = Skywars.getPlugin();
    private List<String> alive = new ArrayList<String>();
    private List<String> spectators = new ArrayList<String>();

    private List<Location> spawns = new ArrayList<Location>();

    private Map<String, Integer> kills = new HashMap<String, Integer>();
    private List<Block> blocks = new ArrayList<Block>();

    public List<Location> getSpawns() {
        return spawns;
    }

    public void loadSpawns() {
        for (String s : plugin.getConfig().getStringList("spawns")) {
            String[] split = s.split(",");
            spawns.add(new Location(Bukkit.getWorld(split[0]), Double
                    .valueOf(split[1]), Double.valueOf(split[2]), Double
                    .valueOf(split[3])));
        }
    }

    private void addItemsInConfig() {
        List<String> items = new ArrayList<String>();
        items.add("272:1");
        items.add("306:1");
        items.add("320:5");
        items.add("261:1");
        items.add("262:3");
        items.add("262:10");

        List<String> opItems = new ArrayList<String>();
        opItems.add("276:1");
        opItems.add("310:1");
        opItems.add("320:5");
        opItems.add("261:1");
        opItems.add("262:3");
        opItems.add("262:10");
        opItems.add("311:1");

        if (plugin.getItems().getStringList("opItems").isEmpty()
                || plugin.getItems().getStringList("opItems") == null) {
            plugin.getItems().set("opItems", opItems);
            plugin.saveItems();
            plugin.getItems().set("items", items);
            plugin.saveItems();
            return;
        }
    }

    private static GameManager instance;

    private World gameWorld = Bukkit.getWorld("world");

    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    public List<String> getAlive() {
        return alive;
    }

    public void load() {
        if (plugin.getConfig().getString("gameWorld") != null) {
            gameWorld = Bukkit.getWorld(plugin.getConfig().getString(
                    "gameWorld"));
            ServerManager.getInstance().setMap(plugin.getThisServer(),
                    gameWorld.getName().trim());
        }
        loadSpawns();
        addItemsInConfig();
    }

    public void setGameWorld(World world) {
        this.gameWorld = world;
        plugin.getConfig().set("gameWorld", world.getName().trim());
        ServerManager.getInstance().setMap(plugin.getThisServer(),
                world.getName().trim());
    }

    public void start() {
        plugin.setState(GameState.INGAME);
        plugin.setTicks(10 * 60); // Set ten minutes

            for (Player pl : Bukkit.getOnlinePlayers()) {
                kills.put(pl.getName(), 0);
                Death.gameKills.put(pl, 0);
                pl.setHealth(pl.getMaxHealth());
                GameScoreboard.setGameScoreboard(pl);
                fillRandomContents(true);
                fillRandomContents(false);
                alive.add(pl.getName());
        }
        if (ServerManager.getInstance().getType(Skywars.getPlugin().getThisServer()).equalsIgnoreCase("solo")) {
            distribute(new ArrayList<Player>(Bukkit.getOnlinePlayers()), spawns);
        }
        else {
            TeamManager.getInstance().setup();
            TeamManager.getInstance().distribute();
        }
    }

    public void removePlayer(Player pl, boolean quit) {
        if (quit) {
            if (plugin.getState() == GameState.INGAME) {
                int a = alive.size();
                if (alive.contains(pl.getName())) {
                    alive.remove(pl.getName());
                } else {
                    spectators.remove(pl.getName());
                }
                if (a > 1) {
                    return;
                } else {
                    stop(true);
                }
            }
        }
    }

    public void stop(Player player) {
        Bukkit.broadcastMessage(player.getName() + " has won the game!");
        new BukkitRunnable() {
            public void run() {
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.performCommand("/leave");
                }
            }
        }.runTaskLater(plugin, 5 * 20L);
    }

    public void stop(boolean winner) {
        Skywars.getPlugin().setState(GameState.JOIN);
        Skywars.getPlugin().setTicks(60);
        if (winner) {
            Player player = Bukkit.getPlayer(alive.get(0));
            Bukkit.broadcastMessage(player.getName() + " has won the game!");

            new BukkitRunnable() {
                public void run() {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.performCommand("leave");
                    }
                }
            }.runTaskLater(plugin, 5 * 20L);
        } else {
            stop(sortPlayers());
        }
    }

    public Player sortPlayers() {
        int mostKills = 0;
        Player winner = null;
        for (String name : alive) {
            if (kills.containsKey(name)) {
                if (kills.get(name) > mostKills) {
                    mostKills = kills.get(name);
                    winner = Bukkit.getPlayer(name);
                }
            }
        }
        return winner;
    }

    public void distribute(List<Player> players, List<Location> locations) {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).teleport(locations.get(i));
        }
    }

    public void playRefillEffect() {
        for (Chunk en : Bukkit.getWorld("world").getLoadedChunks()) {
            for (BlockState entity : en.getTileEntities()) {
                if (entity instanceof Chest) {
                    ParticleEffect refill = new ParticleEffect(
                            ParticleType.BLOCK_DUST, 0.5, 25, 0.0009);
                    refill.sendToLocation(entity.getLocation());
                    entity.getWorld().playSound(entity.getLocation(),
                            Sound.ANVIL_LAND, 1, 1);
                    entity.getWorld().strikeLightningEffect(
                            entity.getLocation());
                }
            }
        }
    }

    public void finishGame(World world) {
        for (Entity en : world.getEntities()) {
            en.remove();
        }
        for (Block block : blocks) {
            block.getState().update(true);
        }
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "corpsremover");
        for (Player online : Bukkit.getOnlinePlayers()) {
            online.performCommand("/leave");
        }
    }

    public void fillRandomContents(boolean op) {
        Random random = new Random();
        if (op) {
            List<String> opItems = Skywars.getPlugin().getItems()
                    .getStringList("opItems");
            for (Chunk chunk : Bukkit.getWorld("world").getLoadedChunks()) {
                for (BlockState tile : chunk.getTileEntities()) {
                    if (tile.getType() == Material.TRAPPED_CHEST) {
                        Chest chest = (Chest) tile;
                        chest.getInventory().clear();
                        for (int i = 0; i < 5; i++) {
                            int ranInt = random.nextInt(opItems.size() - 1);
                            ItemStack item = new ItemStack(
                                    Material.getMaterial(Integer
                                            .valueOf(opItems.get(ranInt).split(
                                                    ":")[0])),
                                    Integer.valueOf(opItems.get(ranInt).split(
                                            ":")[1]));
                            int newRanInt = random.nextInt(chest.getInventory()
                                    .getSize() - 1);
                            chest.getInventory().setItem(newRanInt, item);

                        }
                    }
                }
            }
            Bukkit.broadcastMessage("Middle chests have been refilled!");
        } else {
            List<String> items = Skywars.getPlugin().getItems()
                    .getStringList("items");
            for (Chunk chunk : Bukkit.getWorld("world").getLoadedChunks()) {
                for (BlockState tile : chunk.getTileEntities()) {
                    if (tile.getType() == Material.CHEST) {
                        Chest chest = (Chest) tile;
                        chest.getInventory().clear();
                        for (int i = 0; i < 5; i++) {
                            int ranInt = random.nextInt(items.size() - 1);
                            ItemStack item = new ItemStack(
                                    Material.getMaterial(Integer.valueOf(items
                                            .get(ranInt).split(":")[0])),
                                    Integer.valueOf(items.get(ranInt)
                                            .split(":")[1]));
                            int newRanInt = random.nextInt(chest.getInventory()
                                    .getSize() - 1);
                            chest.getInventory().setItem(newRanInt, item);

                        }
                    }
                }
            }
            Bukkit.broadcastMessage("Island chests have been refilled!");
        }
    }

    public List<Block> getChangedBlocks() {
        return blocks;
    }

    public World getGameWorld() {
        return gameWorld;
    }
}
