package me.xxmatthdxx.skywars.cmds;

import java.util.List;

import me.xxmatthdxx.skywars.Skywars;
import me.xxmatthdxx.skywars.game.GameManager;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameCommand implements CommandExecutor {

	private Skywars plugin = Skywars.getPlugin();

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("game")){
			if(!(sender instanceof Player)){
				sender.sendMessage("Player only command!");
				return true;
			}
			
			Player pl = (Player) sender;

			if(!pl.isOp()){
				pl.sendMessage(ChatColor.RED + "Op only command!");
				return false;
			}
			
			if(args.length == 0){
				pl.sendMessage(ChatColor.DARK_PURPLE + "" + ChatColor.STRIKETHROUGH + "===============" + ChatColor.RED  + "[" + ChatColor.GOLD + "Sky Wars" + ChatColor.RED + "]" + ChatColor.DARK_PURPLE + "" + ChatColor.STRIKETHROUGH + "===============");
				pl.sendMessage(ChatColor.GREEN + "/game addspawn");
				pl.sendMessage(ChatColor.GREEN + "/game delspawn");
				pl.sendMessage(ChatColor.GREEN + "/game forcestart");
				pl.sendMessage(ChatColor.GREEN + "/game stop");
				pl.sendMessage(ChatColor.GREEN + "/game version");
				pl.sendMessage(ChatColor.DARK_PURPLE + "" + ChatColor.STRIKETHROUGH + "==========================================");
			}
			//game set gameMap
			//game addSpawn
			else if(args.length == 1){
				if(args[0].equalsIgnoreCase("addspawn")){
					Location loc = pl.getLocation();
					double x = loc.getX();
					double y = loc.getY();
					double z = loc.getZ();
					World world = loc.getWorld();
					
					if(!world.getName().equalsIgnoreCase(GameManager.getInstance().getGameWorld().getName())){
						pl.sendMessage("You cannot set spawns outside of the game world!");
					}
					else {
						List<String> spawns = plugin.getConfig().getStringList("spawns");
						spawns.add(world.getName().trim() + "," + x + "," + y + "," + z);
						plugin.getConfig().set("spawns", spawns);
						plugin.saveConfig();
						GameManager.getInstance().getSpawns().add(loc);
						pl.sendMessage(ChatColor.GREEN + "Added a spawn to the game!");
						return true;
					}
				}
				else if(args[0].equalsIgnoreCase("forcestop")){
					GameManager.getInstance().stop(false);
					pl.sendMessage("Sending players to hub!");
					return true;
				}
				else if(args[0].equalsIgnoreCase("stop")){
					GameManager.getInstance().stop(GameManager.getInstance().sortPlayers());
					pl.sendMessage("Stopping the game.");
					return true;
				}
			}
		}
		return false;
	}

}
