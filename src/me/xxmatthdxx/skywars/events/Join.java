package me.xxmatthdxx.skywars.events;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.skywars.Skywars;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Join implements Listener {
	
	private String getPlayerCount() {
		return ChatColor.DARK_GRAY + " (" + ChatColor.RED + Bukkit.getOnlinePlayers().size() + ChatColor.DARK_GRAY + "/" + ChatColor.RED + Bukkit.getMaxPlayers() + ChatColor.DARK_GRAY + ")";
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.setJoinMessage(null);
		e.getPlayer().getInventory().clear();
		if(Skywars.getPlugin().getState() == GameState.JOIN) {
			e.setJoinMessage(Skywars.prefix + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.GRAY + " joined! " + getPlayerCount());
		} else {
			e.getPlayer().performCommand("/leave");
		}
	}

}
