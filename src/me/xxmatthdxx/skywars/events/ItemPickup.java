package me.xxmatthdxx.skywars.events;

import me.xxmatthdxx.skywars.spectating.Spectator;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by Matthew on 2015-09-26.
 */
public class ItemPickup implements Listener {

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e){
        if(Spectator.isSpectator(e.getPlayer())){
            e.setCancelled(true);
        }
    }
}
