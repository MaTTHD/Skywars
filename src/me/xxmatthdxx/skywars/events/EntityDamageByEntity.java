package me.xxmatthdxx.skywars.events;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.skywars.Skywars;
import me.xxmatthdxx.skywars.Particles.ParticleEffect;
import me.xxmatthdxx.skywars.Particles.ParticleEffect.ParticleType;
import me.xxmatthdxx.skywars.spectating.Spectator;

import me.xxmatthdxx.skywars.team.TeamManager;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

public class EntityDamageByEntity implements Listener {

    @EventHandler
    public void onHit(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            if (!(e.getDamager() instanceof Player)) return;
            Player pl = (Player) e.getEntity();

            if (TeamManager.getInstance().isOnSameTeam(pl, (Player) e.getDamager())){
                return;
            }

                if (Spectator.isSpectator(pl)) {
                    e.setCancelled(true);
                    Vector boost = pl.getLocation().getDirection().add(new Vector(0, 5, 0));
                    pl.setVelocity(boost.multiply(2)); //Pushes player away
                }
            if (Skywars.getPlugin().getState() == GameState.JOIN) {
                e.setCancelled(true);
                ParticleEffect effect = new ParticleEffect(ParticleType.CLOUD, 0.1, 15, 0.015);
                effect.sendToLocation(e.getEntity().getLocation());

            } else {
                e.setCancelled(false);
            }
        }
    }
}
