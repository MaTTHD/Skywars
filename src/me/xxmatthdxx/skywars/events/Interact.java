package me.xxmatthdxx.skywars.events;



import me.xxmatthdxx.skywars.Skywars;
import me.xxmatthdxx.skywars.game.GameManager;
import me.xxmatthdxx.skywars.spectating.Spectator;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Interact implements Listener {
	
	
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Action action = e.getAction();
		if(action == Action.RIGHT_CLICK_BLOCK){
			Block block = e.getClickedBlock();
			if(block.getType() == Material.TRAPPED_CHEST){
				
			}
		}
		else if(action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
			if (e.getPlayer().getInventory().getItemInHand().getType() == Material.COMPASS) {
				if (Spectator.getCompass().containsKey(e.getPlayer().getName())) {
					if (Spectator.getCompass().get(e.getPlayer().getName()) >= GameManager.getInstance().getAlive().size()) {
						Spectator.getCompass().put(e.getPlayer().getName(), 0);
					} else {
						e.getPlayer().teleport(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(Spectator.getCompass().get(e.getPlayer().getName()))));
						Spectator.getCompass().put(e.getPlayer().getName(), Spectator.getCompass().get(e.getPlayer().getName()) + 1);
						Player pl = Bukkit.getPlayer(GameManager.getInstance().getAlive().get(Spectator.getCompass().get(e.getPlayer().getName())));
						e.getPlayer().sendMessage(Skywars.prefix + ChatColor.GRAY + "You have been teleported to " + ChatColor.GOLD + pl.getName() + "!");
						e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 1);
					}
				}
			} else if (e.getPlayer().getInventory().getItemInHand().getType() == Material.GRASS) {
				e.getPlayer().performCommand("leave");
			}
		}
	}
}
