package me.xxmatthdxx.skywars.events;

import java.util.ArrayList;
import java.util.List;

import me.xxmatthdxx.skywars.game.GameManager;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class ExplodeEvent implements Listener {
		
	@EventHandler
	public void onExplode(EntityExplodeEvent e){
		for(Block block : e.blockList()){
			GameManager.getInstance().getChangedBlocks().add(block);
		}
	}
}
