package me.xxmatthdxx.skywars.events;



import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.skywars.Skywars;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener { 
	
	
	private String getPlayerCount() {
		return ChatColor.DARK_GRAY + " (" + ChatColor.RED + Bukkit.getOnlinePlayers().size() + ChatColor.DARK_GRAY + "/" + ChatColor.RED + Bukkit.getMaxPlayers() + ChatColor.DARK_GRAY + ")";
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		e.setQuitMessage(null);
		if (Skywars.getPlugin().getState() == GameState.INGAME) { 
			e.setQuitMessage(Skywars.prefix + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.GRAY + " suicided!");
			e.getPlayer().setHealth(0);
		} else {
			e.setQuitMessage(Skywars.prefix + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.GRAY + " left! " + getPlayerCount());
		}
	}

}
