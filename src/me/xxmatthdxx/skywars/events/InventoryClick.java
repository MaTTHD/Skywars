package me.xxmatthdxx.skywars.events;

import me.xxmatthdxx.skywars.spectating.Spectator;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClick implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (Spectator.isSpectator(p)) {
			e.setCancelled(true);
		}
	}

}
