package me.xxmatthdxx.skywars.events;

import java.util.ArrayList;
import java.util.HashMap;

import me.xxmatthdxx.nebular.player.PlayerManager;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.skywars.Skywars;
import me.xxmatthdxx.skywars.game.GameManager;
import me.xxmatthdxx.skywars.spectating.Spectator;
import me.xxmatthdxx.skywars.team.Team;
import me.xxmatthdxx.skywars.team.TeamManager;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;

import org.apache.logging.log4j.core.jmx.Server;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.Main;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.scheduler.BukkitRunnable;

public class Death implements Listener {

    private Skywars plugin = Skywars.getPlugin();

    public static HashMap<Player, Integer> gameKills = new HashMap<Player, Integer>();

    @EventHandler
    public void onDeath(final PlayerDeathEvent e) {
        if (!(e.getEntity().getKiller() instanceof Player)) {
            // TODO sucicide.
            Player p = e.getEntity();

            if (ServerManager.getInstance().getType(plugin.getThisServer()).equalsIgnoreCase("team")) {
                if (TeamManager.getInstance().hasTeam(p)) {
                    Team t = TeamManager.getInstance().getTeam(p);
                    if ((t.getPlayers().size() - 1) <= 0) {
                        Bukkit.broadcastMessage(ChatColor.GREEN + org.bukkit.ChatColor.BOLD.toString() + "Team " + t.getName() + " has been eliminated!");
                    }
                }
                if (GameManager.getInstance().getAlive().size() == 2) {
                    if (TeamManager.getInstance().isOnSameTeam(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0)), Bukkit.getPlayer(GameManager.getInstance().getAlive().get(1)))) {

                        for (Player pl : Bukkit.getOnlinePlayers()) {
                            pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            pl.sendMessage(" ");
                            pl.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                    + "            " + TeamManager.getInstance().getTeam(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0))).getName() + " WON!");
                            pl.sendMessage(" ");
                            pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            pl.playSound(pl.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                        }
                        Team winner = TeamManager.getInstance().getTeam(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0)));
                        GameManager.getInstance().stop(true);
                        for (String s : winner.getPlayers()) {
                            Player killer = Bukkit.getPlayer(s);

                            if (!killer.hasPermission("skywars.donor")) {
                                killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                        + "------------------------------");
                                killer.sendMessage(" ");
                                killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                        + "            GAME WON!");
                                killer.sendMessage(ChatColor.AQUA + "       +50 Coins for winning!");
                                killer.sendMessage(" ");
                                killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                        + "------------------------------");
                                PlayerManager.getInstance().addCurrency(killer, 50);
                                killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);

                            } else {
                                killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                        + "------------------------------");
                                killer.sendMessage(" ");
                                killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                        + "            GAME WON!");
                                killer.sendMessage(ChatColor.AQUA
                                        + "       +100 Coins for winning!");
                                killer.sendMessage(" ");
                                killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                        + "------------------------------");
                                PlayerManager.getInstance().addCurrency(killer, 100);
                                killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                            }
                        }
                    }
                } else if (GameManager.getInstance().getAlive().size() <= 1) {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        pl.sendMessage(" ");
                        pl.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                + "            " + TeamManager.getInstance().getTeam(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0))).getName() + " WON!");
                        pl.sendMessage(" ");
                        pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        pl.playSound(pl.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                    }
                    Team winner = TeamManager.getInstance().getTeam(Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0)));
                    GameManager.getInstance().stop(true);
                    for (String s : winner.getPlayers()) {
                        Player killer = Bukkit.getPlayer(s);

                        if (!killer.hasPermission("skywars.donor")) {
                            killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            killer.sendMessage(" ");
                            killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                    + "            GAME WON!");
                            killer.sendMessage(ChatColor.AQUA + "       +50 Coins for winning!");
                            killer.sendMessage(" ");
                            killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            PlayerManager.getInstance().addCurrency(killer, 50);
                            killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);

                        } else {
                            killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            killer.sendMessage(" ");
                            killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                    + "            GAME WON!");
                            killer.sendMessage(ChatColor.AQUA
                                    + "       +100 Coins for winning!");
                            killer.sendMessage(" ");
                            killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                    + "------------------------------");
                            PlayerManager.getInstance().addCurrency(killer, 100);
                            killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                        }
                    }
                } else {

                }

                e.setDeathMessage(e.getEntity().getName() + "Suicided!");
                Spectator.setSpectatorMode(p);
                GameManager.getInstance().getAlive().remove(p.getName());
                if (GameManager.getInstance().getAlive().size() <= 1) {
                    for (Player pl : Bukkit.getOnlinePlayers()) {
                        pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        pl.sendMessage(" ");
                        pl.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                + "            " + GameManager.getInstance().getAlive().get(0) + " WON!");
                        pl.sendMessage(" ");
                        pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        pl.playSound(pl.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                    }
                    Player killer = Bukkit.getPlayer(GameManager.getInstance().getAlive().get(0));
                    GameManager.getInstance().stop(true);
                    if (!killer.hasPermission("skywars.donor")) {
                        killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        killer.sendMessage(" ");
                        killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                + "            GAME WON!");
                        killer.sendMessage(ChatColor.AQUA + "       +50 Coins for winning!");
                        killer.sendMessage(" ");
                        killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        PlayerManager.getInstance().addCurrency(killer, 50);
                        killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);

                    } else {
                        killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        killer.sendMessage(" ");
                        killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                                + "            GAME WON!");
                        killer.sendMessage(ChatColor.AQUA
                                + "       +100 Coins for winning!");
                        killer.sendMessage(" ");
                        killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                                + "------------------------------");
                        PlayerManager.getInstance().addCurrency(killer, 100);
                        killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                    }
                }
                return;
            }

            e.setDeathMessage("");

            Player killer = e.getEntity().getKiller();
            Player p = e.getEntity().getPlayer();

            gameKills.put(killer, gameKills.get(killer) + 1);
            Spectator.setSpectatorMode(p);
            GameManager.getInstance().

                    getAlive().remove(p.getName());

            if (GameManager.getInstance().getAlive().size() <= 1) {
                for (String name : GameManager.getInstance().getAlive()) {
                    Player pl = Bukkit.getPlayer(name);
                    pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    pl.sendMessage(" ");
                    pl.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                            + "            " + GameManager.getInstance().getAlive().get(0) + " WON!");
                    pl.sendMessage(" ");
                    pl.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    pl.playSound(pl.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                }
                GameManager.getInstance().stop(true);
                if (!killer.hasPermission("skywars.donor")) {
                    killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    killer.sendMessage(" ");
                    killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                            + "            GAME WON!");
                    killer.sendMessage(ChatColor.AQUA + "       +50 Coins for winning!");
                    killer.sendMessage(" ");
                    killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    PlayerManager.getInstance().addCurrency(killer, 50);
                    killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);

                } else {
                    killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    killer.sendMessage(" ");
                    killer.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD
                            + "            GAME WON!");
                    killer.sendMessage(ChatColor.AQUA
                            + "       +100 Coins for winning!");
                    killer.sendMessage(" ");
                    killer.sendMessage(ChatColor.GOLD + "" + ChatColor.STRIKETHROUGH
                            + "------------------------------");
                    PlayerManager.getInstance().addCurrency(killer, 100);
                    killer.playSound(killer.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);

                }
            } else

            {

            }

            final PacketPlayInClientCommand packet = new PacketPlayInClientCommand(
                    EnumClientCommand.PERFORM_RESPAWN);
            Player pl = (Player) e.getEntity();
            final CraftPlayer craftPl = (CraftPlayer) pl;

            new

                    BukkitRunnable() {
                        public void run() {
                            craftPl.getHandle().playerConnection.a(packet);
                        }
                    }

                    .

                            runTaskLater(plugin, 10L);

        }

    public static int getTotalGameKills(Player p) {
        return gameKills.get(p);
    }

}
