package me.xxmatthdxx.skywars;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import me.xxmatthdxx.nebular.game.GameState;
import me.xxmatthdxx.nebular.server.PlayableServer;
import me.xxmatthdxx.nebular.server.ServerManager;
import me.xxmatthdxx.nebular.stat.Stat;
import me.xxmatthdxx.nebular.stat.StatManager;
import me.xxmatthdxx.skywars.cmds.GameCommand;
import me.xxmatthdxx.skywars.events.Death;
import me.xxmatthdxx.skywars.events.EntityDamageByEntity;
import me.xxmatthdxx.skywars.events.Interact;
import me.xxmatthdxx.skywars.events.InventoryClick;
import me.xxmatthdxx.skywars.events.Join;
import me.xxmatthdxx.skywars.events.Quit;
import me.xxmatthdxx.skywars.game.GameManager;
import me.xxmatthdxx.skywars.game.GameTimer;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Skywars extends JavaPlugin {
	
	private static Skywars plugin;

	/**
	 * Kits/upgrades
	 * Fix knocked into void
	 * Change sign format.
	 * Add team support.
	 */
	
	private PlayableServer server;
	private GameState state;
	private int ticks;
	
	private final int LOBBY_TIME = 2*60;
	private final int GAME_TIME = 10*60;
	
	public static String prefix = ChatColor.GREEN + "" + ChatColor.BOLD + "Skywars > ";
	
	private FileConfiguration items;
	private File itemFile;
	
	public void onEnable(){
		plugin = this;
		server = ServerManager.getInstance().getServer(Bukkit.getServer().getServerName());
		itemFile = new File(getDataFolder(), "items.yml");
		if(!itemFile.exists()){
			try {
				itemFile.createNewFile();
			} catch(Exception e){
				
			}
			items = YamlConfiguration.loadConfiguration(itemFile);
		}
		items = YamlConfiguration.loadConfiguration(itemFile);
		getConfig().options().copyDefaults(true);
		saveConfig();
		registerEvents();
		registerCommands();
		createStats();
		GameManager.getInstance().load();
		ticks = 60;
		
		GameTimer timer = new GameTimer();
		timer.runTaskTimer(this, 0L, 20L);
		ServerManager.getInstance().setState(server, GameState.JOIN);
		state = GameState.JOIN;
	}
	
	public void createStats(){
		Stat[] statList = {new Stat("Skywars", "Kills"), new Stat("Skywars", "Deaths"), new Stat("Skywars", "Wins")};
		StatManager.getInstance().addStats(Arrays.asList(statList));
	}
	
	public void registerEvents(){
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Death(), this);
		pm.registerEvents(new EntityDamageByEntity(), this);
		pm.registerEvents(new Interact(), this);
		pm.registerEvents(new InventoryClick(), this);
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new Quit(), this);
	}
	
	public void registerCommands(){
		GameCommand gc = new GameCommand();
		getCommand("game").setExecutor(gc);
	}
	
	public void onDisable(){
		
	}
	
	public static Skywars getPlugin(){
		return plugin;
	}
	
	public GameState getState(){
		return state;
	}
	
	public void setState(GameState state){
		this.state = state;
		ServerManager.getInstance().setState(server, state);
	}
	
	public int getTicks(){
		return ticks;
	}
	
	public void setTicks(int ticks){
		this.ticks = ticks;
	}
	
	public FileConfiguration getItems(){
		return items;
	}
	
	public void saveItems(){
		try {
			items.save(itemFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PlayableServer getThisServer(){
		return server;
	}
}
